# openwrt-HC5861

#### 介绍
OpenWrt For HiWiFi(HC5861) 自编译精减固件，极路由3自用固件

**v19.07.2**

1. 支持 NTFS 读写
2. 支持 Wi-Fi 5G 驱动
3. 默认开启 WiFi
4. 不支持 Fat 文件系统
5. 默认语言为中文
6. 支持 samba 共享
7. 支持 Luci 挂载点
8. 支持 LED 灯
9. SD卡驱动、USB 2.0 驱动

#### 安装教程
1) 将 `HC5861-uboot.bin` 和 `XXXXXX-openwrt-ramips-mt7620a-hiwifi-hc5761-squashfs-sysupgrade.bin` 上传到路由器 `/tmp` 目录中

2) 将U-boot替换成解锁版

```
mtd write /tmp/HC5861-uboot.bin u-boot
```

3) 刷入固件

```
sysupgrade -F -n /tmp/XXXXXX-openwrt-ramips-mt7620a-hiwifi-hc5761-squashfs-sysupgrade.bin
```

4) 等待刷完系统自动重启，刷入过程切勿断电，否则变砖. (首次重启需要比较长，请耐心等待)

#### 可能出现问题

##### 1. 确认账号和密码正确情况下，拨号上网失败

解决方法：通过 ssh 连接路由器，执行以下命令

```
cd /etc/config
uci set network.lan.ifname='eth0'
uci set network.wan.ifname='eth0.1'
uci commit network
reboot
```

